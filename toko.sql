-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 22, 2020 at 12:22 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `toko`
--

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(20) NOT NULL,
  `nama_kategori` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Pulau'),
(2, 'Pantai'),
(3, 'Gunung'),
(4, 'Desa/Suku');

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id_order` int(50) NOT NULL,
  `nama1` varchar(50) NOT NULL,
  `nama2` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `kota` varchar(50) NOT NULL,
  `kode_pos` varchar(50) NOT NULL,
  `no_hp` varchar(50) NOT NULL,
  `date` varchar(200) NOT NULL,
  `price` varchar(120) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id_order`, `nama1`, `nama2`, `email`, `alamat`, `kota`, `kode_pos`, `no_hp`, `date`, `price`, `nama`, `status`) VALUES
(39, 'Fikih', 'Alan', 'pikiha52@gmail.com', 'Sumur bandung 3', 'Depok', '1431', '082201552612', '2020-07-22', '12.000.000', 'Pulau Tidung', 'waiting');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id_product` int(100) NOT NULL,
  `id_kategori` int(20) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `caption` varchar(100) NOT NULL,
  `harga` varchar(100) NOT NULL,
  `gambar` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id_product`, `id_kategori`, `nama`, `caption`, `harga`, `gambar`) VALUES
(40, 1, 'Pulau Tidung', '3 Day<br>2 Night', '12000000', 'tidung1.png'),
(41, 3, 'Bukit Sikunir Wonosobo', 'Paket 4 Orang<br>\r\n5Day 4Night', '2000000', 'sikunir.jpg'),
(42, 2, 'Pantai Kuta Bali', 'Paket 2 Orang<br>\r\n4Day 3Night', '3000000', 'kuta.jpg'),
(43, 2, 'Pantai Santolo Garut', 'Paket 4 Orang<br>\r\n3Day 2Night', '950000', 'pantaisantolo.jpg'),
(44, 1, 'Pulau Pahawang Lampung ', 'Paket 6 Orang<br>\r\n4Day 3Night', '1000000', 'pahawang.jpg'),
(45, 1, 'Pulau Macan Resort', 'Paket 2 Orang<br>\r\nKhusus 5Day 4Night', '8000000', 'pulaumacan.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `product_home`
--

CREATE TABLE `product_home` (
  `id` int(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `caption` varchar(100) NOT NULL,
  `gambar` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_home`
--

INSERT INTO `product_home` (`id`, `nama`, `caption`, `gambar`) VALUES
(10, 'Pulau Tidung', '2d1n/3d2n', 'tidung.png'),
(11, 'Bukit Sikunir Wonosobo', '2d1n', 'sikunir.jpg'),
(12, 'Pantai Kuta Bali', '3d2n/5d4n', 'kuta.jpg'),
(13, 'Pantai Santolo Garut', '3d2n', 'pantaisantolo.jpg'),
(14, 'Gunung Bromo', '3d2n/5d4n', 'bromo.jpeg'),
(15, 'Baduy Luar', '3d2n/5d4n', 'baduy.png'),
(16, 'Pulau Pahawang Lampung', '3d2n/5d4n', 'pahawang.jpg'),
(17, 'Pulau Belitung', '3d2n', 'belitung.jpg'),
(18, 'Pulau Macan Resort', '3d2n/5d4n', 'pulaumacan.jpg'),
(19, 'Pulau Pari', '3d2n', 'pulaupari.jpeg'),
(20, 'Pulau Pisang Lampung', '3d2n/5d2n', 'pulaupisang.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(20) NOT NULL,
  `role` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(100) NOT NULL,
  `username` varchar(44) NOT NULL,
  `password` varchar(200) NOT NULL,
  `email` varchar(50) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `role_id` int(20) NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `photo` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_active` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `username`, `password`, `email`, `full_name`, `phone`, `role_id`, `last_login`, `photo`, `created_at`, `is_active`) VALUES
(11, 'admin', '$2y$10$o36KxetujHWtBAIkVH0BLed7uvy1JbmwgM6WXjvMd1eZDQleXsVeO', 'admin@gmail.com', 'Fikih alan', '082210552612', 1, '2020-07-11 17:54:00', 'pp.jpeg', '0000-00-00 00:00:00', 1),
(12, 'user', '$2y$10$o36KxetujHWtBAIkVH0BLed7uvy1JbmwgM6WXjvMd1eZDQleXsVeO', 'user@gmail.com', 'Fikih Alan', '082210552612', 2, '2020-07-12 16:49:57', 'default.jpg', '2020-06-08 17:00:00', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id_order`),
  ADD UNIQUE KEY `status` (`id_order`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id_product`);

--
-- Indexes for table `product_home`
--
ALTER TABLE `product_home`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id_order` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id_product` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `product_home`
--
ALTER TABLE `product_home`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
