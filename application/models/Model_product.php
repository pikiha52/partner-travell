<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_product extends CI_Model{
    public function all(){
        $hasil = $this->db->get('product');
        if($hasil->num_rows() > 0){
            return $hasil->result();
        } else {
            return array();
        }
    }

    public function allHome(){
        $hasil = $this->db->get('product_home');
        if($hasil->num_rows() > 0){
            return $hasil->result();
        } else {
            return array();
        }
    }

    public function find($id_product){
        //mecari query id di db
        $hasil = $this->db->where('id_product',$id_product)
        ->limit(1)
        ->get('product');
        if($hasil->num_rows() > 0){
            return $hasil->row();
        } else {
            return array();
        }
    }

    public function addPaket($tabel, $data)
    {
        return $this->db->insert($tabel, $data);
    }

    public function delete_home($id)
    {
        $query = $this->db->query("
        DELETE FROM `product_home` 
        WHERE
        `product_home`.`id` = $id
       ");
        return $query;
    }

}