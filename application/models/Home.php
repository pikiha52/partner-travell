<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Model{
    public function all(){
        $hasil = $this->db->get('product_home');
        if($hasil->num_rows() > 0){
            return $hasil->result();
        } else {
            return array();
        }
    }
}