<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Product extends CI_Model{
private $table = 'product';
private $primary_key = 'id_product';
    public function all()
    {
        $hasil = $this->db->get('product');
        if($hasil->num_rows() > 0){
            return $hasil->result();
        } else {
            return array();
        }
    }

    public function getId($id)
{
   return $this->db->get_where($this->table, array($this->primary_key => $id))->result();
}

public function edit($id, $data)
{
   $this->db->update($this->table, $data, array($this->primary_key => $id));
}

public function insert($data)
{
   $this->db->insert($this->table, $data);
}

public function delete($id)
{
$this->db->delete($this->table, array($this->primary_key => $id));
}

    public function search($keyword)
    {
        $this->db->like('nama', $keyword);
        $this->db->or_like('harga', $keyword);

        $result = $this->db->get('product')->result();
        return $result;
    }
}