<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_crooz extends CI_Model{
    public function find($id_kategori){
        //mecari query id di db
        $hasil = $this->db->where('id_kategori',$id_kategori)
        ->limit(1)
        ->get('product');
        if($hasil->num_rows() > 0){
            return $hasil->row();
        } else {
            return array();
        }
    }

    public function all()
    {
        $hasil = $this->db->get('product');
        if($hasil->num_rows() > 0){
            return $hasil->result();
        } else {
            return array();
        }
    }
}