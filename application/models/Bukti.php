<?php
class Bukti extends CI_Model{
private $table = 'bukti_tf';
private $primary_key = 'id';

   function selectAll()
   {
		$this->db->order_by("id_order","asc"); 
		return $this->db->get('bukti_tf')->result();
   }

   public function addBukti($table, $data)
   {
       return $this->db->insert($table, $data);
   }

   public function delete($id)
{
$this->db->delete($this->table, array($this->primary_key => $id));
}
}
?>