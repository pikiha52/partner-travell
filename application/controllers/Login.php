<?php
class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model("login_model");
    }

    public function index()
    {   
        $data['title'] = 'Partner Travell - Login';
        

        $this->load->view("auth/login", $data);
    
}
public function proses_login()
{   
    $this->form_validation->set_rules('email', 'Email', 'required');
    $this->form_validation->set_rules('password', 'Password', 'required');

    if ($this->form_validation->run() == FALSE) {

        $errors = $this->form_validation->error_array();
        $this->session->set_flashdata('errors', $errors);
        $this->session->set_flashdata('input', $this->input->post());
        redirect('login');
     
    } else {

        $email = htmlspecialchars($this->input->post('email'));
        $pass = htmlspecialchars($this->input->post('password'));
        $cek_login = $this->login_model->cek_login($email);  

        if($cek_login == FALSE)
        {

            $this->session->set_flashdata('message', 'Email yang Anda masukan tidak terdaftar.');
            redirect('Login');

        } else {

            if(password_verify($pass, $cek_login->password)){
                $this->session->set_userdata('user_id', $cek_login->user_id);
                // var_dump($cek_login);
                // die();
                $this->session->set_userdata('full_name', $cek_login->full_name);
                $this->session->set_userdata('email', $cek_login->email);
                $this->session->set_userdata('role_id', $cek_login->role_id);
                $this->session->set_userdata('photo', $cek_login->photo);
                $role_user = $this->session->userdata('role_id');
                if($role_user == "1"){

                    redirect('admin/AdminController');
                } else {
                    redirect('UserController');
                }

            } else {

                $this->session->set_flashdata('message', 'Email atau password yang Anda masukan salah, 
                silahkan lakukan login ulang.');
                redirect('Login');

            }
        }
    }
}


public function register()
{
    $data['title'] ='Partner Travell - Register';

    $this->form_validation->set_rules(
        'email', 'Email',
        'required|is_unique[user.email]',
        array(
                'required'      => 'You have not provided %s.',
                'is_unique'     => 'Email ini sudah ada.'
        )
    );
  
    // $this->form_validation->set_rules('email', 'Email', 'required|trim', 'valid_email|is_unique[user.email]', [
    //     'is_unique' => 'Email ini sudah ada!'
    //     ]);
    $this->form_validation->set_rules('password', 'Password', 'required|trim');
    if ($this->form_validation->run() == false) {
        $this->load->view('auth/register', $data);
    } else {
        $data = [
            'username' => htmlspecialchars($this->input->post('username', true)),
            'phone' => htmlspecialchars($this->input->post('phone', true)),
            'email' => htmlspecialchars($this->input->post('email', true)),
            'full_name' => htmlspecialchars($this->input->post('full_name')),
            'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
            'role_id' => '2',
            'photo' => 'default.jpg',
            'created_at' => time(),
            'is_active' => 1
        ];

        $this->login_model->register('user', $data);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Selamat, akun berhasil dibuat,
         silahkan login.</div>');
        redirect('login');
    }
}




public function logout()
{
    $this->session->sess_destroy();
    echo '<script>
        alert("Sukses! Anda berhasil logout."); 
        window.location.href="'.base_url('Login').'";
        </script>';
}

}
