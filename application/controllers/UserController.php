<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_product');
        $this->load->model('Model_crooz');
        $this->load->model('Order');
        $this->load->library('form_validation');
        $this->load->model('Upload');
         $this->load->library('upload');

        $this->STATUS = array(
      'statususer' => 'null',
            
    );
    }
    

    public function map()
    {
        $data['title'] = "Lokasi Kami";
        $this->load->view('templates/header', $data);
        $this->load->view('project/map');
    }
    
    public function contact()
    {
        $data['title'] = "Contact me";
        $this->load->view('templates/header', $data);
        $this->load->view('project/contact');
    }


    public function index()
    {
        $data['title'] = 'Partner Travell';
        $data['project'] = $this->Home->all();

        $this->load->view('templates/header', $data);
        $this->load->view('project/index', $data);
        $this->load->view('templates/footer');
    }


        public function upload()
        {
            $this->load->view('templates/header');
            $this->load->view('project/upload');
            // $this->load->view('templates/footer');
        }


    public function detail()
    {
        $data['title'] = 'Partner Travell';

        $this->load->view('templates/header',$data);
        $this->load->view('project/detail-product');
        $this->load->view('templates/footer');
    }


}