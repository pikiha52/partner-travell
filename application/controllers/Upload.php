<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
		$this->load->model('Order');
		$this->load->model('Pengguna');
      
    }

	function create()
	{
        $this->load->view('templates/header');
        $this->load->view('project/upload');
        $this->load->view('templates/footer');
	}

	function proses()
	{
		$data['Order'] = $this->Order->getAll();
		$data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
		$config['upload_path']          = './img/upload/';
		$config['allowed_types']        = 'gif|jpg|png';
		$config['max_size']             = 100;
		$config['max_width']            = 2048;
		$config['max_height']           = 768;
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload('file'))
		{
				$error = array('error' => $this->upload->display_errors());
				$this->load->view('templates/header', $data);
				$this->load->view('project/profile', $error);
				$this->load->view('templates/footer');
		}
		else
		{
			$image_data = $this->upload->data();
			$imgdata = file_get_contents($image_data['full_path']);
			$file_encode=base64_encode($imgdata);
			$data['id_order'] = $this->input->post('id_order');
            $data['email'] = $this->input->post('email');
			$data['tipe_file'] = $this->upload->data('file_type');
			$data['ukuran'] = $this->upload->data('file_size');
			$data['file'] = $file_encode;
			$data['nama_file'] =  $this->upload->data('file_name');
			$this->db->insert('upload',$data);
			unlink($image_data['full_path']);
			redirect('upload');
		}
    }
    
    public function index()
    {
        $data['title'] = 'Partner Travell';
        $data['Order'] = $this->Order->getAll();
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $this->load->view('templates/header',$data);
        $this->load->view('project/profile',$data);
        // $this->load->view('templates/footer');
    }

}
