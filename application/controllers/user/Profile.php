<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->model('order');
		$this->load->model('pengguna');
      
    }
 
    public function index()
    {
		$order = $this->session->userdata('user_id');
		$order = $this->order->selectAllById($order);
		$data = [
			'title' => 'Partner Travel - My profile',
			'order' => $order
		];
// var_dump($order);
// die();

        $this->load->view('templates/header',$data);
        $this->load->view('project/profile');
        $this->load->view('templates/footer');
    }
   
    function proses()
	{
	$config['upload_path']          = './img/upload/';
    $config['allowed_types']        = 'gif|jpg|png|jpeg';
    $config['max_size']             = 100;
    $config['max_width']            = 2048;
    $config['max_height']           = 768;
    $this->load->library('upload', $config);
    if ( ! $this->upload->do_upload('file'))
    {
        
            $image_data = $this->upload->data();
			$imgdata = file_get_contents($image_data['full_path']);
			$file_encode=base64_encode($imgdata);
			$data['id_order'] = $this->input->post('id_order');
            $data['email'] = $this->input->post('email');
           	// $data['file'] = $file_encode;
			$data['gambar'] =  $this->upload->data('file_name');
			$this->db->insert('bukti_tf',$data);
			unlink($image_data['full_path']);
			redirect('user/profile');
		}
	}

}