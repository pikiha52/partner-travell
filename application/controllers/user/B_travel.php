<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class b_travel extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_product');
    }

public function index()
    {
       
       $product = $this->model_product->all();
        $data = [
           'title' => 'Partner Travel - Booking travell',
           'product' => $product
       ];

       $this->load->view('templates/header',$data);
    //    $this->load->view('templates/sidebar_kategori');
        $this->load->view('project/travel/booking');
       $this->load->view('templates/footer');
    }
}