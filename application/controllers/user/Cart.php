<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_product');
        $this->load->model('order');
        $this->load->library('form_validation');
    }
        

public function index()
{
   $data['title'] = 'Booking Travel';
   $this->load->view('templates/header', $data);
    $this->load->view('project/show_cart',$data);
    // $this->load->view('templates/footer');
    
}

public function add_to_cart($id_product)
{
    $product = $this->model_product->find($id_product);
    $data = array(
        'id' => $product->id_product,
        'image' => $product->gambar,
        'qty' => 1,
        'price' => $product->harga,
        'name' => $product->nama,
        'caption' => $product->caption
    );
    $this->cart->insert($data);
    redirect(base_url('user/cart'));
}

public function clear_cart()
{
    $this->cart->destroy();
    redirect(base_url('user/cart'));
}

    public function order()
    {
        $this->form_validation->set_rules('no_hp', 'no_hp', 'required|trim');
        $this->form_validation->set_rules('date', 'date', 'required|trim');
        $this->form_validation->set_rules('kode_pos', 'kode_pos', 'required|trim');
        if ($this->form_validation->run() == FALSE) {
            $data = [
                'title' => 'Partner Travell'
            ];

            $this->load->view('templates/header', $data);
            $this->load->view('project/checkout');
        } else {

            $data = [
                'title' => 'Partner Travell - Isi data booking'
            ];
            
            $id = $this->session->userdata('user_id');
            $full_name = $this->session->userdata('full_name');
            $email = $this->session->userdata('email');
            $alamat = $this->input->post('alamat');
            $kota = $this->input->post('kota');
            $kode_pos = $this->input->post('kode_pos');
            $no_hp = $this->input->post('no_hp');
            $date = $this->input->post('date');
            $price = $this->input->post('price');
            $nama = $this->input->post('nama');
    
            $data = [
                'id_user' => $id,
                'full_name' => $full_name,
                'email' => $email,
                'alamat' => $alamat,
                'kota' => $kota,
                'kode_pos' => $kode_pos,
                'no_hp' => $no_hp,
                'date' => $date,
                'status' => 'waiting',
                'nama' => $nama,
                'price' => $price,            
                
            ];
    
            $insert = $this->order->insert("order", $data); 
                  
        
            if($insert){

            $this->cart->destroy();
            $this->session->set_flashdata('success_order', 'Booking telah berhasil.');
            redirect('user/Profile');

            }
        }
    }
}