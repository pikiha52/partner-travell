<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller
{
    public function index()
    {
        $data['project'] = $this->Home->all();
        $data['title'] = 'Partner Travell';

        $this->load->view('templates/header_awal', $data);
        $this->load->view('project/awal', $data);
        // $this->load->view('templates/footer');
    }
    

    public function map()
    {
        $data['title'] = "Lokasi Kami";
        $this->load->view('templates/header_awal', $data);
        $this->load->view('project/map');
    }
    
    public function contact()
    {
        $data['title'] = "Contact me";
        $this->load->view('templates/header_awal', $data);
        $this->load->view('project/contact');
	}
	
}