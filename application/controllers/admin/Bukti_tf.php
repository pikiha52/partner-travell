<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bukti_tf extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('bukti');
    }

public function index()
    {

        $bukti = $this->bukti->selectAll();
        $data = [
            'title' => 'Partner Travel - Bukti transfer',
            'bukti' => $bukti
        ];

        $this->load->view('v_admin/admin_header', $data);
        $this->load->view('v_admin/admin_sidebar');
        $this->load->view('v_admin/bukti');
        $this->load->view('v_admin/admin_footer');
    }

    public function delete($id)
    {
        $this->bukti->delete($id);
        $this->session->set_flashdata('pesan', '<script>alert("Data Berhasil DIhapus")</script>');
        redirect(base_url('adminn/bukti_tf'));
    }

}