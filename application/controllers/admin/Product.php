<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller
{
 
    public function index()
    {
        $data['title'] = 'Tukang Kaos';
        $data['model_product'] = $this->Model_product->all();
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

       $this->load->view('admin/admin_header',$data);
       $this->load->view('admin/admin_sidebar');
        $this->load->view('admin/product',$data);
       $this->load->view('admin/admin_footer');
    }

    function proses()
	{
		// $data['Order'] = $this->Order->getAll();
		// $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
		$config['upload_path']          = './img/upload/';
		$config['allowed_types']        = 'gif|jpg|png';
		$config['max_size']             = 100;
		$config['max_width']            = 2048;
		$config['max_height']           = 768;
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload('file'))
		{
				$error = array('error' => $this->upload->display_errors());
                $this->load->view('admin/admin_header');
                $this->load->view('admin/admin_sidebar');
                $this->load->view('admin/product',$error);
                $this->load->view('admin/admin_footer');
		}
		else
		{
			$image_data = $this->upload->data();
			$imgdata = file_get_contents($image_data['full_path']);
			$file_encode=base64_encode($imgdata);
			$data['id_kategori'] = $this->input->post('id_kategori');
            $data['nama'] = $this->input->post('nama');
            $data['caption'] = $this->input->post('caption');
            $data['harga'] = $this->input->post('harga');
			$data['gambar'] =  $this->upload->data('file_name');
			$this->db->insert('product',$data);
			unlink($image_data['full_path']);
			redirect('product');
		}
    }

}