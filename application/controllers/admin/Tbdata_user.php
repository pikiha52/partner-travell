<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tbdata_user extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('pengguna');
    }

public function index()
    {
        $pengguna = $this->pengguna->selectAll();
        $data = [
            'title' => 'Partner Travell - Daftar user',
            'pengguna' => $pengguna
        ];
        
        $this->load->view('v_admin/admin_header',$data);
        $this->load->view('v_admin/admin_sidebar');
        $this->load->view('v_admin/general-table');
        $this->load->view('v_admin/admin_footer');
    }


    public function edit_user()
    {
        $id = $this->input->post('user_id');
        $fullname = $this->input->post('full_name');
        $username = $this->input->post('username');            
        $email = $this->input->post('email');
        $role_id = $this->input->post('role_id');
        $phone = $this->input->post('phone');
        $password = $this->input->post('password');
        $pass = password_hash($password, PASSWORD_DEFAULT);
        
        $data = array(
            'full_name' => $fullname,
            'username' => $username,
            'email' => $email,
            'role_id' => $role_id,
            'phone' => $phone,
            'password' => $pass
             );

            $where = array(
                'user_id' => $id
            );

            $this->pengguna->update_data($where, $data, 'user');
            $this->session->set_flashdata('message_user', '<div class="alert alert-success role="alert">Data user berhasil diubah</div>');
            redirect('admin/Tbdata_user');

        }
    


    public function delete($id)
{
    $this->Pengguna->delete($id);
    $this->session->set_flashdata('pesan', '<script>alert("Data Berhasil DIhapus")</script>');
    redirect(base_url('adminn/tbdata_user'));
}

}