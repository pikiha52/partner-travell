<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('pengguna');
    }


public function index()
    {
        {

                $id_user = $this->session->userdata('user_id');
                $profile= $this->pengguna->selectAllByPengguna($id_user);  
            
                    // $absensi = $absensi = $this->Absensi_model->selectAll($month, $year);
                $data = [
                    'title' => 'Nine Parking | Laporan Harian',
                    'profile' => $profile
                ];

// var_dump($profile);
// die();

            $this->load->view('v_admin/admin_header',$data);
            $this->load->view('v_admin/admin_sidebar');
            $this->load->view('v_admin/profile',$data);
            $this->load->view('v_admin/admin_footer');
        }
    }

    function edit_profile()
{
    $id = $this->input->post('user_id');
    $_image = $this->db->get_where('user',['user_id' => $id])->row();
    $config['upload_path']          = './img/profile/';
    $config['allowed_types']        = 'gif|jpg|png|jpeg';
    $config['max_size']             = 0;
    $config['max_width']            = 0;
    $config['max_height']           = 0;
    $this->load->library('upload', $config);
   
    if (!$this->upload->do_upload('photo')){
 
             $data = array(
            'full_name'=> $this->input->post('full_name'),
            'username'=> $this->input->post('username'),
            'email'=> $this->input->post('email'),
            'phone'=> $this->input->post('phone'),
            'instagram' => $this->input->post('instagram'),
            'facebook' => $this->input->post('facebook'),
            'twitter' => $this->input->post('twitter'),
            'biografi' => $this->input->post('biografi'),
            );

            // var_dump($data);
            // die();

            $query = $this->db->update('user', $data, array('user_id' => $id));;
            $this->session->set_flashdata('message_profil_admin', '<div class="alert alert-success role="alert">Profile berhasil diupdate</div>');
            redirect(base_url('admin/Profile'));      

                }else{
        $_data = array('upload_data' => $this->upload->data());
         $data = array(
            'full_name'=> $this->input->post('full_name'),
            'username'=> $this->input->post('username'),
            'email'=> $this->input->post('email'),
            'phone'=> $this->input->post('phone'),
            'instagram' => $this->input->post('instagram'),
            'facebook' => $this->input->post('facebook'),
            'twitter' => $this->input->post('twitter'),
            'biografi' => $this->input->post('biografi'),
            'photo' => $_data['upload_data']['file_name']
            );

            // var_dump($data);
            // die();

            $query = $this->db->update('user', $data, array('user_id' => $id));;
            $this->session->set_flashdata('message_profile_admin', '<div class="alert alert-success role="alert">Profile berhasil diupdate</div>');
            redirect(base_url('admin/Profile'));      
          }
    }

}
        