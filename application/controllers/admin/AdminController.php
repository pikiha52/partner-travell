<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Order');
        $this->load->model('model_product');
        
    }

    public function index()
    {
        $user = $this->session->userdata('email');
        $order = $this->Order->getAll();
        $data = [
            'title' => 'Partner Travell - Admin',
            'order' => $order,
            'user' => $user
        ];
     
        $this->load->view('v_admin/admin_header', $data);
        $this->load->view('v_admin/admin_sidebar');
        $this->load->view('v_admin/index');
        $this->load->view('v_admin/admin_footer');
    }

    public function product()
    {
        $product = $this->model_product->all();
        $data = [
                'title' => 'Partner Travell - Paket Travell',
                'product' => $product
        ];
        

       $this->load->view('v_admin/admin_header',$data);
       $this->load->view('v_admin/admin_sidebar');
       $this->load->view('v_admin/product');
       $this->load->view('v_admin/admin_footer');
    }

    public function do_upload()
    {
        $product = $this->model_product->all();
        $data = [
                'title => Partner Travell - Paket Travell',
                'product' => $product
        ];
        
$config['upload_path']          = './img/travel-img/';
$config['allowed_types']        = 'gif|jpg|png|jpeg';
$config['max_size']             = 0;
$config['max_width']            = 0;
$config['max_height']           = 0;
$this->load->library('upload', $config);
if (!$this->upload->do_upload('gambar')){
        $error = array('error' => $this->upload->display_errors());
        $this->load->view('v_admin/admin_header',$data);
        $this->load->view('v_admin/admin_sidebar');
         $this->load->view('v_admin/product',$error);
        $this->load->view('v_admin/admin_footer');
}else{
    $_data = array('upload_data' => $this->upload->data());
     $data = array(
        'id_kategori'=> $this->input->post('id_kategori'),
        'nama'=> $this->input->post('nama'),
        'caption'=> $this->input->post('caption'),
        'harga'=> $this->input->post('harga'),
        'gambar' => $_data['upload_data']['file_name']
        );

        // var_dump($data);
        // die();
    $insert = $this->model_product->addPaket('product',$data);
    if($insert){
        echo 'berhasil di upload';
        redirect('admin/AdminController/product');
    }else{
        echo 'gagal upload';
    }
}
}

        
    public function edit_order()
{
    $id = $this->input->post('id_order');
    $nama1 = $this->input->post('nama1');
    $nama2 = $this->input->post('nama2');
    $email = $this->input->post('email');
    $alamat = $this->input->post('alamat');            
    $kota = $this->input->post('kota');
    $no_hp = $this->input->post('no_hp');
    $date = $this->input->post('date');
    $status = $this->input->post('status');

    
    $data = array(
        'nama1' => $nama1,
        'nama2' => $nama2,
        'email' => $email,
        'alamat' => $alamat,
        'kota' => $kota,
        'no_hp' => $no_hp,
        'date' => $date,
        'status' => $status
         );

        $where = array(
            'id_order' => $id
        );

        $this->Order->update_data($where, $data, 'order');
        $this->session->set_flashdata('message', '<div class="alert alert-success role="alert">Data order berhasil diupdate.</div>');
        redirect('admin/AdminController');
}

public function edit_home()
{
    $home = $this->model_product->allHome();
    $data = [
        'title' => 'Partner Travel - Edit home',
        'home' => $home
    ];

    // $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

   $this->load->view('v_admin/admin_header',$data);
   $this->load->view('v_admin/admin_sidebar');
    $this->load->view('v_admin/product_home');
   $this->load->view('v_admin/admin_footer');
}

function edit_home_fc()
{
    $id = $this->input->post('id');
    $_image = $this->db->get_where('product_home',['id' => $id])->row();
    $config['upload_path']          = './img/travel-img/';
    $config['allowed_types']        = 'gif|jpg|png|jpeg';
    $config['max_size']             = 0;
    $config['max_width']            = 0;
    $config['max_height']           = 0;
    $this->load->library('upload', $config);
   
    if (!$this->upload->do_upload('gambar')){
 
             $data = array(
            'nama'=> $this->input->post('nama'),
            'caption'=> $this->input->post('caption'),
            );

            // var_dump($data);
            // die();

            $query = $this->db->update('product_home', $data, array('id' => $id));;
            $this->session->set_flashdata('message', '<div class="alert alert-success role="alert">Paket booking berhasil diubah</div>');
            redirect(base_url('admin/AdminController/edit_home'));      

                }else{
        $_data = array('upload_data' => $this->upload->data());
         $data = array(
            'nama'=> $this->input->post('nama'),
            'caption'=> $this->input->post('caption'),
            'gambar' => $_data['upload_data']['file_name']
            );

            // var_dump($data);
            // die();

            $query = $this->db->update('product_home', $data, array('id' => $id));;
            $this->session->set_flashdata('message', '<div class="alert alert-success role="alert">Paket booking berhasil diubah</div>');
            redirect(base_url('admin/AdminController/edit_home'));      
          }
    }


    public function upload_home()
    {
        $product = $this->model_product->allHome();
        $data = [
                'title => Partner Travell - Paket Travell',
                'product' => $product
        ];
        
$config['upload_path']          = './img/travel-img/';
$config['allowed_types']        = 'gif|jpg|png|jpeg';
$config['max_size']             = 0;
$config['max_width']            = 0;
$config['max_height']           = 0;
$this->load->library('upload', $config);
if (!$this->upload->do_upload('gambar')){
        $error = array('error' => $this->upload->display_errors());
        $this->load->view('v_admin/admin_header',$data);
        $this->load->view('v_admin/admin_sidebar');
         $this->load->view('v_admin/product_home',$error);
        $this->load->view('v_admin/admin_footer');
}else{
    $_data = array('upload_data' => $this->upload->data());
     $data = array(
        'nama'=> $this->input->post('nama'),
        'caption'=> $this->input->post('caption'),
        'gambar' => $_data['upload_data']['file_name']
        );

        // var_dump($data);
        // die();
    $insert = $this->model_product->addPaket('product_home',$data);
    if($insert){
        echo 'berhasil di upload';
        redirect('admin/AdminController/edit_home');
    }else{
        echo 'gagal upload';
    }
}
}

    function insert()
{
    $config['upload_path']          = './img/upload/';
    $config['allowed_types']        = 'gif|jpg|png|jpeg';
    $config['max_size']             = 100;
    $config['max_width']            = 2048;
    $config['max_height']           = 768;
    $this->load->library('upload', $config);
    if ( ! $this->upload->do_upload('file'))
    {
        
            $image_data = $this->upload->data();
			$imgdata = file_get_contents($image_data['full_path']);
			$file_encode=base64_encode($imgdata);
			$data['nama'] = $this->input->post('nama');
            $data['harga'] = $this->input->post('harga');
            $data['caption'] = $this->input->post('caption');
            $data['id_kategori'] = $this->input->post('id_kategori');
			$data['tipe_file'] = $this->upload->data('file_type');
			$data['ukuran'] = $this->upload->data('file_size');
			$data['file'] = $file_encode;
			$data['gambar'] =  $this->upload->data('file_name');
			$this->db->insert('product',$data);
			unlink($image_data['full_path']);
			redirect('admin/AdminController/product');
		}
    }


function update_product()
{
    $id = $this->input->post('id_product');
    $_image = $this->db->get_where('product',['id_product' => $id])->row();
    $config['upload_path']          = './img/travel-img/';
    $config['allowed_types']        = 'gif|jpg|png|jpeg';
    $config['max_size']             = 0;
    $config['max_width']            = 0;
    $config['max_height']           = 0;
    $this->load->library('upload', $config);
   
    if (!$this->upload->do_upload('gambar')){
 
             $data = array(
            'nama'=> $this->input->post('nama'),
            'caption'=> $this->input->post('caption'),
            'harga'=> $this->input->post('harga'),
            );

            // var_dump($data);
            // die();

            $query = $this->db->update('product', $data, array('id_product' => $id));;
            $this->session->set_flashdata('message_product', '<div class="alert alert-success role="alert">Paket booking berhasil diubah</div>');
            redirect(base_url('admin/AdminController/product'));      

                }else{
        $_data = array('upload_data' => $this->upload->data());
         $data = array(
            'nama'=> $this->input->post('nama'),
            'caption'=> $this->input->post('caption'),
            'harga'=> $this->input->post('harga'),
            'gambar' => $_data['upload_data']['file_name']
            );

            // var_dump($data);
            // die();

            $query = $this->db->update('product', $data, array('id_product' => $id));;
            $this->session->set_flashdata('message_product', '<div class="alert alert-success role="alert">Paket booking berhasil diubah</div>');
            redirect(base_url('admin/AdminController/product'));      
          }
    }

public function delete($id)
{
    $this->Order->delete($id);
    $this->session->set_flashdata('message_order', '<script>alert("Data Berhasil Dihapus")</script>');
    redirect(base_url('admin/AdminController'));
}

public function delete_product($id)
{
    $this->Product->delete($id);
    $this->session->set_flashdata('message_product', '<script>alert("Data Berhasil Dihapus")</script>');
    redirect(base_url('admin/AdminController/product'));
}

public function delete_home($id)
{
    $this->model_product->delete_home($id);
    $this->session->set_flashdata('message', '<script>alert("Data Berhasil Dihapus")</script>');
    redirect(base_url('admin/AdminController/edit_home'));
}
}