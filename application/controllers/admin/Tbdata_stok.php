<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tbdata_stok extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('order');
    }
 

public function index()
    {

        $daftar = $this->order->getAll();
        $data = [
                'title' => 'Partner Travel - Daftar paket booking',
                'daftar' => $daftar 
        ];

        $this->load->view('v_admin/admin_header',$data);
        $this->load->view('v_admin/admin_sidebar');
        $this->load->view('v_admin/data-table');
        $this->load->view('v_admin/admin_footer');
    }

}