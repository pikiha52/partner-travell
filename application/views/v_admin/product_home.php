<div class="dashboard-wrapper">
	<div class="dashboard-ecommerce">
		<div class="container-fluid dashboard-content ">
			<!-- ============================================================== -->
			<!-- pageheader  -->
			<!-- ============================================================== -->
			<div class="row">
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<div class="page-header">
                    <?= $this->session->flashdata('message'); ?>
                        <h2 class="pageheader-title">Daftar Paket Booking in Home </h2>
						<div class="page-breadcrumb">
							<nav aria-label="breadcrumb">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Partner Travell</a></li>
									<li class="breadcrumb-item active" aria-current="page">Daftar Paket Booking in Home</li>
								</ol>
							</nav>
						</div>
					</div>
				</div>
			</div>
			<!-- ============================================================== -->
			<!-- end pageheader  -->
			<!-- ============================================================== -->
            <a data-toggle="modal" data-target="#addModal" class="btn btn-outline-primary">Add New</a>
            <br>
            <br>
			<?php
//Columns must be a factor of 12 (1,2,3,4,6,12)
$numOfCols = 4;
$rowCount = 0;
$bootstrapColWidth = 12 / $numOfCols;
?>
			<div class="row">
				<?php
foreach ($home as $p){
?>
				<div class="col-md-<?php echo $bootstrapColWidth; ?>">
					<div class="thumbnail">

						<div class="product-thumbnail">
							<div class="product-img-head">
								<div class="product-img">
									<img src="<?php echo base_url().'img/travel-img/' .$p->gambar;?>" alt=""
										class="img-fluid"></div>
							</div>
							<div class="product-content">
								<div class="product-content-head">
									<h3 class="product-title"><?=$p->nama?></h3>
                                    <p class="product-title"><?=$p->caption?></p>
                                </div>
                                
								<div class="product-btn">
									<a type="button" class="btn btn-outline-primary" data-toggle="modal"
										data-target="#editModal<?php echo $p->id?>">Edit</a>

									<a onclick="deleteConfirm('<?php echo site_url('admin/AdminController/delete_home/'.$p->id) ?>')"
										href="#!" class="btn btn-outline-danger">
										Hapus</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php
    $rowCount++;
    if($rowCount % $numOfCols == 0) echo '</div><div class="row">';
}
?>
			</div>
		</div>
	</div>
	<!-- Modal Add Product-->
	<!-- <?php echo $error; ?> -->
	<?php echo form_open_multipart('admin/AdminController/upload_home');?>
	<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
		aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Tambah Paket</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Name</label>
						<input class="form-control" name="nama" type="text">
						<p class="text-red"><?php echo form_error('nama'); ?></p>
					</div>
					<div class="form-group">
						<label>Image</label>
						<input class="form-control" name="gambar" type="file">
						<p class="text-red"><?php echo form_error('gambar'); ?></p>
					</div>
					<div class="form-group">
						<label>Caption</label>
						<input class="form-control" name="caption" type="text">
						<p class="text-red"><?php echo form_error('caption'); ?></p>
					</div>
					<!-- <div class="form-group">
						<label>Id kategori</label>
						<input class="form-control" name="id" type="text">
						<p class="text-red"><?php echo form_error('id'); ?></p>
					</div> -->
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" value="upload" class="btn btn-primary">Save</button>
				</div>
			</div>
		</div>
	</div>

	</form>


	<!-- Edit Modal-->
	<?php $no = 0;
   foreach ($home as $p) : $no++; ?>
	<div class="modal fade" id="editModal<?php echo $p->id?>" tabindex="-1" role="dialog"
		aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form action="<?php echo base_url(). 'admin/AdminController/edit_home_fc'; ?>" enctype="multipart/form-data" method="post">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Edit paket travel in Home</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="form-group">
                            <label>Name</label>
                            <input class="form-control" name="id" type="hidden" value="<?php echo $p->id ?>">
							<input class="form-control" name="nama" type="text" value="<?php echo $p->nama ?>">
						</div>
						<div class="form-group">
							<label>Image</label>
							<input class="form-control" name="gambar" type="file">
						</div>
						<div class="form-group">
							<label>Caption</label>
							<input class="form-control" name="caption" type="text" value="<?php echo $p->caption ?>">
						</div>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="submit" value="upload" class="btn btn-primary">Save</button>
					</div>
				</form>
			</div>
		</div>
	</div>


	<?php endforeach;?>
	<!-- End Edit Modal-->


	<!-- Logout Delete Confirmation-->
	<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
		aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Are you sure?</h5>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">Data yang dihapus tidak akan bisa dikembalikan.</div>
				<div class="modal-footer">
					<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
					<a id="btn-delete" class="btn btn-danger" href="#">Delete</a>
				</div>
			</div>
		</div>
	</div>
	<script>
		function deleteConfirm(url) {
			$('#btn-delete').attr('href', url);
			$('#deleteModal').modal();
		}

	</script>
