<div><?php echo $this->session->flashdata('message');?></div>
<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
	<div class="dashboard-ecommerce">
		<div class="container-fluid dashboard-content ">
			<!-- ============================================================== -->
			<!-- pageheader  -->
			<!-- ============================================================== -->
			<div class="row">
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<div class="page-header">
						<h2 class="pageheader-title">Admin Partner Travell </h2>
						<div class="page-breadcrumb">
							<nav aria-label="breadcrumb">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Partner Travell</a>
									</li>
									<!-- <li class="breadcrumb-item active" aria-current="page">Bekerja jujur</li> -->
								</ol>
							</nav>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<!-- ============================================================== -->
				<!-- sales  -->
				<!-- ============================================================== -->
				<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
					<div class="card border-3 border-top border-top-primary">
						<div class="card-body">
							<h5 class="text-muted">Sales</h5>
							<div class="metric-value d-inline-block">
								<h1 class="mb-1">$12099</h1>
							</div>
							<div class="metric-label d-inline-block float-right text-success font-weight-bold">
								<span class="icon-circle-small icon-box-xs text-success bg-success-light"><i
										class="fa fa-fw fa-arrow-up"></i></span><span class="ml-1">5.86%</span>
							</div>
						</div>
					</div>
				</div>
				<!-- ============================================================== -->
				<!-- end sales  -->
				<!-- ============================================================== -->
				<!-- ============================================================== -->
				<!-- new customer  -->
				<!-- ============================================================== -->
				<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
					<div class="card border-3 border-top border-top-primary">
						<div class="card-body">
							<h5 class="text-muted">New Customer</h5>
							<div class="metric-value d-inline-block">
								<h1 class="mb-1">1245</h1>
							</div>
							<div class="metric-label d-inline-block float-right text-success font-weight-bold">
								<span class="icon-circle-small icon-box-xs text-success bg-success-light"><i
										class="fa fa-fw fa-arrow-up"></i></span><span class="ml-1">10%</span>
							</div>
						</div>
					</div>
				</div>
				<!-- ============================================================== -->
				<!-- end new customer  -->
				<!-- ============================================================== -->
				<!-- ============================================================== -->
				<!-- visitor  -->
				<!-- ============================================================== -->
				<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
					<div class="card border-3 border-top border-top-primary">
						<div class="card-body">
							<h5 class="text-muted">Visitor</h5>
							<div class="metric-value d-inline-block">
								<h1 class="mb-1">13000</h1>
							</div>
							<div class="metric-label d-inline-block float-right text-success font-weight-bold">
								<span class="icon-circle-small icon-box-xs text-success bg-success-light"><i
										class="fa fa-fw fa-arrow-up"></i></span><span class="ml-1">5%</span>
							</div>
						</div>
					</div>
				</div>
				<!-- ============================================================== -->
				<!-- end visitor  -->
				<!-- ============================================================== -->
				<!-- ============================================================== -->
				<!-- total orders  -->
				<!-- ============================================================== -->
				<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
					<div class="card border-3 border-top border-top-primary">
						<div class="card-body">
							<h5 class="text-muted">Total Orders</h5>
							<div class="metric-value d-inline-block">
								<h1 class="mb-1">1340</h1>
							</div>
							<div class="metric-label d-inline-block float-right text-danger font-weight-bold">
								<span
									class="icon-circle-small icon-box-xs text-danger bg-danger-light bg-danger-light "><i
										class="fa fa-fw fa-arrow-down"></i></span><span class="ml-1">4%</span>
							</div>
						</div>
					</div>
				</div>
				<!-- ============================================================== -->
				<!-- end total orders  -->
				<!-- ============================================================== -->
			</div>
			<!-- ============================================================== -->
			<!-- end pageheader  -->
			<!-- ============================================================== -->
			<div class="ecommerce-widget">
				<div class="row">
					<!-- ============================================================== -->

					<!-- ============================================================== -->

					<!-- recent orders  -->
					<!-- ============================================================== -->
					<div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
						<div class="card">
							<h5 class="card-header">Daftar Booking</h5>
							<div class="card-body p-0">
								<div class="table-responsive">
									<table class="table">
										<tbody>
											<th scope="col">No</th>
											<th scope="col">Id order</th>
											<th scope="col">Tujuan</th>
											<th scope="col">Nama</th>
											<th scope="col">Email</th>
											<th scope="col">Alamat</th>
											<th scope="col">Kota</th>
											<th scope="col">Kode pos</th>
											<th scope="col">No hp</th>
											<th scope="col">Tanggal booking</th>
											<th scope="col">Harga</th>
											<th scope="col">Status</th>
											<th scope="col">Action</th>
											<tr>
												<?php $i = 1 ;?>
												<?php foreach($order as $o) : ?>

											<tr>
												<th scope="col"><?=$i;?></th>
												<td><?php echo $o->id_order ?></td>
												<td><?php echo $o->nama ?></td>
												<td><?php echo $o->full_name ?>
												<td><?php echo $o->email ?></td>
												<td><?php echo $o->alamat ?></td>
												<td><?php echo $o->kota ?></td>
												<td><?php echo $o->kode_pos ?></td>
												<td><?php echo $o->no_hp ?></td>
												<td><?php echo $o->date ?></td>
												<td><?php echo $o->price ?></td>
												<td><?php echo $o->status ?></td>
												<td colspan="12">
													<a type="button" class="btn btn-light" data-toggle="modal"
														data-target="#editmodal<?php echo $o->id_order?>"><i
															class="fas fa-edit"></i> Edit</a></td>
											</tr>
											<?php $i++;?>
											<?php endforeach; ?>
											</tr>
											<!-- <tr>
											</tr> -->
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- Edit Modal-->

<?php $no = 0;
   foreach ($order as $o) : $no++; ?>
<div class="modal fade" id="editmodal<?php echo $o->id_order?>" tabindex="-1" role="dialog"
	aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="<?php echo base_url(). 'admin/AdminController/edit_order'; ?>" method="post">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Order edit</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Nama</label>
						<input class="form-control" name="id_order" type="hidden" value="<?php echo $o->id_order ?>">
						<input class="form-control" name="nama1" type="text" value="<?php echo $o->full_name ?>">
					</div>
					<div class="form-group">
						<label>Email</label>
						<input class="form-control" name="email" type="email" value="<?php echo $o->email ?>">
					</div>
					<div class="form-group">
						<label>Alamat</label>
						<input class="form-control" name="alamat" type="text" value="<?php echo $o->alamat ?>">
					</div>
					<div class="form-group">
						<label>Kota</label>
						<input class="form-control" name="kota" type="text" value="<?php echo $o->kota ?>">
					</div>
					<div class="form-group">
						<label>Kode pos</label>
						<input class="form-control" name="kode_pos" type="text" value="<?php echo $o->kode_pos ?>">
					</div>
					<div class="form-group">
						<label>Nomor handphone</label>
						<input class="form-control" name="no_hp" type="text" value="<?php echo $o->no_hp ?>">
					</div>
					<div class="form-group">
						<label>Tanggal booking</label>
						<input class="form-control" name="date" type="date" value="<?php echo $o->date ?>">
					</div>
					<div class="form-group">
						<label for="inputDivisi4">Status</label>
						<select class="form-control" name="status">
							<option class="hidden" value="<?php echo $o->status ?>"><?php echo $o->status ?>
							</option>
							<option>waiting</option>
							<option>on progres</option>
							<option>finish</option>
						</select>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" value="upload" class="btn btn-primary">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>


<?php endforeach;?>

<!-- End Edit Modal-->
