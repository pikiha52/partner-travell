<div class="dashboard-wrapper">
	<div class="container-fluid  dashboard-content">
		<!-- ============================================================== -->
		<!-- pageheader -->
		<!-- ============================================================== -->
		<div class="row">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<div class="page-header">
				<?= $this->session->flashdata('message_user'); ?>
					<h2 class="pageheader-title">List Data</h2>
					<div class="page-breadcrumb">
						<nav aria-label="breadcrumb">
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Partner Travell</a></li>
								<li class="breadcrumb-item"><a href="#" class="breadcrumb-link">List Data</a></li>
								<li class="breadcrumb-item active" aria-current="page">Data user</li>
							</ol>
						</nav>
					</div>
				</div>
			</div>
		</div>
		<!-- ============================================================== -->
		<!-- end pageheader -->
		<!-- ============================================================== -->

		<div class="row">
			<!-- ============================================================== -->
			<!-- hoverable table -->
			<!-- ============================================================== -->
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<div><?php echo $this->session->flashdata('pesan');?></div>
				<div class="card">
					<h5 class="card-header">Data user</h5>
					<div class="card-body">
						<table class="table table-hover">
							<thead>
								<tr>
									<th scope="col">No</th>
									<th scope="col">Username</th>
									<th scope="col">Email</th>
									<th scope="col">Fullname</th>
									<th scope="col">Role</th>
									<th scope="col">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php $i = 1; ?>
								<?php foreach($pengguna as $u) : ?>
								<tr>
									<td><?=$i;?></td>
									<td><?php echo $u->username ?></td>
									<td><?php echo $u->email ?></td>
									<td><?php echo $u->full_name ?></td>
									<td><?php echo $u->role_id ?></td>
									<td>
										<div class="box-footer">
										<a type="button" class="btn btn-primary" data-toggle="modal" data-target="#editmodal<?php echo $u->user_id?>">Edit</a>
											<a onclick="deleteConfirm('<?php echo site_url('admin/tbdata_user/delete/'.$u->user_id) ?>')"
												href="#!" class="btn btn-small text-danger"><i class="fas fa-trash"></i>
												Hapus</a>
										</div>
									</td>
								</tr>
								<?php $i++ ; ?>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- ============================================================== -->
			<!-- end hoverable table -->
			<!-- hoverable table -->
			<!-- ============================================================== -->
		</div>
	</div>
</div>


<!-- Edit Modal-->

<?php $no = 0;
   foreach ($pengguna as $u) : $no++; ?>
	<div class="modal fade" id="editmodal<?php echo $u->user_id?>" tabindex="-1" role="dialog"
		aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form action="<?php echo base_url(). 'admin/Tbdata_user/edit_user'; ?>" method="post">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Edit user</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="form-group">
                            <label>Name</label>
                            <input class="form-control" name="user_id" type="hidden" value="<?php echo $u->user_id ?>">
							<input class="form-control" name="full_name" type="text" value="<?php echo $u->full_name ?>">
						</div>
						<div class="form-group">
							<label>Username</label>
							<input class="form-control" name="username" type="text" value="<?php echo $u->username ?>">
						</div>
						<div class="form-group">
							<label>Email</label>
							<input class="form-control" name="email" type="email" value="<?php echo $u->email ?>">
						</div>
						<div class="form-group">
							<label>Role id</label>
							<input class="form-control" name="role_id" type="text" value="<?php echo $u->role_id ?>">
						</div>
						<div class="form-group">
							<label>Number Phone</label>
							<input class="form-control" name="phone" type="text" value="<?php echo $u->phone ?>">
						</div>
						<div class="form-group">
							<label>Password</label>
							<input class="form-control" name="password" type="text" value="">
						</div>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="submit" value="upload" class="btn btn-primary">Save</button>
					</div>
				</form>
			</div>
		</div>
	</div>


	<?php endforeach;?>

<!-- End Edit Modal-->


<!-- Logout Delete Confirmation-->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
	aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Are you sure?</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">Data yang dihapus tidak akan bisa dikembalikan.</div>
			<div class="modal-footer">
				<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
				<a id="btn-delete" class="btn btn-danger" href="#">Delete</a>
			</div>
		</div>
	</div>
</div>
<script>
	function deleteConfirm(url) {
		$('#btn-delete').attr('href', url);
		$('#deleteModal').modal();
	}

</script>
