<div class="dashboard-wrapper">
	<div class="container-fluid  dashboard-content">
		<!-- ============================================================== -->
		<!-- pageheader -->
		<!-- ============================================================== -->
		<div class="row">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<div class="page-header">
					<h2 class="pageheader-title">Report data order</h2>
					<div class="page-breadcrumb">
						<nav aria-label="breadcrumb">
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Partner Travell</a></li>
								<li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Data Report</a></li>
								<li class="breadcrumb-item active" aria-current="page">Report data order</li>
							</ol>
						</nav>
					</div>
				</div>
			</div>
		</div>
		<!-- ============================================================== -->
		<!-- end pageheader -->

		<div class="row">
			<!-- ============================================================== -->
			<!-- data table  -->
			<!-- ============================================================== -->
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<div class="card">
					<div class="card-header">
						<h5 class="mb-0">Report Data Order</h5>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="example" class="table table-striped table-bordered second" style="width:100%">
								<thead>
									<tr>
										<th>No</th>
										<th>Id order</th>
										<th>Name</th>
										<th>Email</th>
										<th>Alamat</th>
										<th>Kota</th>
										<th>Kode pos</th>
										<th>Nomor hp</th>
										<th>Tanggal</th>
										<th>Harga</th>
										<th>Nama paket</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
									<?php $i = 1; ?>
									<?php foreach($daftar as $d) : ?>

									<tr>
										<th scope="row"><?= $i; ?></th>
										<td><?php echo $d->id_order ?></td>
										<td><?php echo $d->full_name ?>
										<td><?php echo $d->email ?></td>
                                        <td><?php echo $d->alamat ?></td>
                                        <td><?php echo $d->kota ?></td>
                                        <td><?php echo $d->kode_pos ?></td>
                                        <td><?php echo $d->no_hp ?></td>
                                        <td><?php echo $d->date ?></td>
                                        <td><?php echo $d->price ?></td>
                                        <td><?php echo $d->nama ?></td>
                                        <td><?php echo $d->status ?></td>
									</tr>
									<?php $i++ ;?>
									<?php endforeach; ?>

								</tbody>
								<tfoot>
									<tr>
										<th>No</th>
										<th>Id order</th>
										<th>Name</th>
										<th>Email</th>
										<th>Alamat</th>
										<th>Kota</th>
										<th>Kode pos</th>
										<th>Nomor hp</th>
										<th>Tanggal</th>
										<th>Harga</th>
										<th>Nama paket</th>
										<th>Status</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- ============================================================== -->
            <!-- end data table  -->
        </div>
    </div>
</div>
			<!-- Optional JavaScript -->
			<script src="<?php echo base_url()?>assets/vendor/jquery/jquery-3.3.1.min.js"></script>
			<script src="<?php echo base_url()?>assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
			<script src="<?php echo base_url()?>assets/vendor/slimscroll/jquery.slimscroll.js"></script>
			<script src="<?php echo base_url()?>assets/vendor/multi-select/js/jquery.multi-select.js"></script>
			<script src="<?php echo base_url()?>assets/libs/js/main-js.js"></script>
			<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
			<script src="<?php echo base_url()?>assets/vendor/datatables/js/dataTables.bootstrap4.min.js"></script>
			<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
			<script src="<?php echo base_url()?>assets/vendor/datatables/js/buttons.bootstrap4.min.js"></script>
			<script src="<?php echo base_url()?>assets/vendor/datatables/js/data-table.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
			<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
			<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
			<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.colVis.min.js"></script>
			<script src="https://cdn.datatables.net/rowgroup/1.0.4/js/dataTables.rowGroup.min.js"></script>
			<script src="https://cdn.datatables.net/select/1.2.7/js/dataTables.select.min.js"></script>
			<script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
