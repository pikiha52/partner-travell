<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<style>
	body {
		background: -webkit-linear-gradient(left, #3931af, #00c6ff);
	}

	.emp-profile {
		padding: 3%;
		margin-top: 3%;
		margin-bottom: 3%;
		border-radius: 0.5rem;
		background: #fff;
	}

	.profile-img {
		text-align: center;
	}

	.profile-img img {
		width: 70%;
		height: 100%;
	}

	.profile-img .file {
		position: relative;
		overflow: hidden;
		margin-top: -20%;
		width: 70%;
		border: none;
		border-radius: 0;
		font-size: 15px;
		background: #212529b8;
	}

	.profile-img .file input {
		position: absolute;
		opacity: 0;
		right: 0;
		top: 0;
	}

	.profile-head h5 {
		color: #333;
	}

	.profile-head h6 {
		color: #0062cc;
	}

	.profile-edit-btn {
		border: none;
		border-radius: 1.5rem;
		width: 70%;
		padding: 2%;
		font-weight: 600;
		color: #6c757d;
		cursor: pointer;
	}

	.proile-rating {
		font-size: 12px;
		color: #818182;
		margin-top: 5%;
	}

	.proile-rating span {
		color: #495057;
		font-size: 15px;
		font-weight: 600;
	}

	.profile-head .nav-tabs {
		margin-bottom: 5%;
	}

	.profile-head .nav-tabs .nav-link {
		font-weight: 600;
		border: none;
	}

	.profile-head .nav-tabs .nav-link.active {
		border: none;
		border-bottom: 2px solid #0062cc;
	}

	.profile-work {
		padding: 14%;
		margin-top: -15%;
	}

	.profile-work p {
		font-size: 12px;
		color: #818182;
		font-weight: 600;
		margin-top: 10%;
	}

	.profile-work a {
		text-decoration: none;
		color: #495057;
		font-weight: 600;
		font-size: 14px;
	}

	.profile-work ul {
		list-style: none;
	}

	.profile-tab label {
		font-weight: 600;
	}

	.profile-tab p {
		font-weight: 600;
		color: #0062cc;
	}

</style>
<!-- ============================================================== -->
<!-- wrapper  -->
<!-- ============================================================== -->
<div class="dashboard-wrapper">
	<div class="dashboard-ecommerce">
		<div class="container-fluid dashboard-content ">
			<div class="container emp-profile">
				<?php foreach($profile as $u) : ?>
				<form method="post">
					<div class="row">
						<div class="col-md-4">
							<div class="profile-img">
								<img src="<?php echo base_url('img/profile/') . $u->photo ?>" alt="" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="profile-head">
								<h5>
									<?php echo $u->full_name ?>
								</h5>
								<h6>
									Admin Partner Travell
								</h6>
								<ul class="nav nav-tabs" id="myTab" role="tablist">
									<li class="nav-item">
										<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home"
											role="tab" aria-controls="home" aria-selected="true">About</a>
									</li>
								</ul>
							</div>
						</div>
						<div class="col-md-2">
							<a type="button" class="btn btn-light" data-toggle="modal" data-target="#editmodal">Edit
								Profile</a>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="profile-work">
								<p>SOCIAL MEDIA</p>
								<a href="<?php echo $u->instagram ?>">Instagram</a><br />
								<a href="<?php echo $u->twitter ?>">Twitter</a><br />
								<a href="<?php echo $u->facebook ?>">Facebook</a>
								<p>BIOGRAFI</p>
								<p>
									<?php echo $u->biografi ?>
								</p>
							</div>
						</div>
						<div class="col-md-8">
							<div class="tab-content profile-tab" id="myTabContent">
								<div class="tab-pane fade show active" id="home" role="tabpanel"
									aria-labelledby="home-tab">
									<div class="row">
										<div class="col-md-6">
											<label>Name</label>
										</div>
										<div class="col-md-6">
											<p><?php echo $u->full_name ?></p>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<label>Email</label>
										</div>
										<div class="col-md-6">
											<p><?php echo $u->email ?></p>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<label>Phone</label>
										</div>
										<div class="col-md-6">
											<p><?php echo $u->phone ?></p>
										</div>
									</div>
									<div class="row">
										<div class="col-md-6">
											<label>Profession</label>
										</div>
										<div class="col-md-6">
											<p>Web Developer and Designer</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php endforeach ;?>
				</form>
			</div>
		</div>
	</div>
</div>


<!-- Edit Modal -->


<div class="modal fade" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
	aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="<?php echo base_url(). 'admin/Profile/edit_profile'; ?>" enctype="multipart/form-data" method="post">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Edit user</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<?php foreach ($profile as $u) : ?>
					<div class="form-group">
						<label>Name</label>
						<input class="form-control" name="user_id" type="hidden" value="<?php echo $u->user_id ?>">
						<input class="form-control" name="full_name" type="text" value="<?php echo $u->full_name ?>">
					</div>
					<div class="form-group">
						<label>Username</label>
						<input class="form-control" name="username" type="text" value="<?php echo $u->username ?>">
					</div>
					<div class="form-group">
						<label>Email</label>
						<input class="form-control" name="email" type="email" value="<?php echo $u->email ?>">
					</div>
					<div class="form-group">
						<label>Photo Profile</label>
						<input class="form-control" name="photo" type="file">
					</div>
					<div class="form-group">
						<label>Number Phone</label>
						<input class="form-control" name="phone" type="text" value="<?php echo $u->phone ?>">
					</div>
					<div class="form-group">
						<label>Instagram<small id="biografiHelpBlock" class="form-text text-muted">
							 Isi dengan link halaman profile akun sosial media anda. 
							</small></label>
						<input class="form-control" name="instagram" type="text" value="<?php echo $u->instagram ?>">
					</div>
					<div class="form-group">
						<label>Twitter<small id="biografiHelpBlock" class="form-text text-muted">
							 Isi dengan link halaman profile akun sosial media anda. 
							</small></label>
						<input class="form-control" name="twitter" type="text" value="<?php echo $u->twitter ?>">
					</div>
					<div class="form-group">
						<label>Instagram<small id="biografiHelpBlock" class="form-text text-muted">
							 Isi dengan link halaman profile akun sosial media anda. 
							</small></label>
						<input class="form-control" name="facebook" type="text" value="<?php echo $u->facebook ?>">
					</div>
					<div class="form-group">
						<label>Biografi<small id="biografiHelpBlock" class="form-text text-muted">
							 Gunakan < br > untuk membuat kalimat baru. 
							</small></label>
						<textarea class="form-control" name="biografi" id="biografi" cols="30"
							rows="10"><?php echo $u->biografi ?></textarea>

					</div>
				</div>
				<?php endforeach;?>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" value="upload" class="btn btn-primary">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>


<!-- End Edit Modal-->


<!-- End edit Modal -->
