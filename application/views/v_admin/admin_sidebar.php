        <!-- ============================================================== -->
        <!-- left sidebar -->
        <!-- ============================================================== -->
        <div class="nav-left-sidebar sidebar-dark">
        	<div class="menu-list">
        		<nav class="navbar navbar-expand-lg navbar-light">
        			<a class="d-xl-none d-lg-none" href="#">Dashboard</a>
        			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
        				aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        				<span class="navbar-toggler-icon"></span>
        			</button>
        			<div class="collapse navbar-collapse" id="navbarNav">
        				<ul class="navbar-nav flex-column">
        					<li class="nav-divider">
        						Menu
        					</li>
        					<li class="nav-item ">
        						<a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false"
        							data-target="#submenu-1" aria-controls="submenu-1"><i
        								class="fa fa-fw fa-user-circle"></i>Dashboard <span
        								class="badge badge-success">6</span></a>
        						<div id="submenu-1" class="collapse submenu">
        							<ul class="nav flex-column">
        								<li class="nav-item">
        									<a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false"
        										data-target="#submenu-1-2" aria-controls="submenu-1-2">Partner Travell</a>
        									<div id="submenu-1-2" class="collapse submenu">
        										<ul class="nav flex-column">
        											<li class="nav-item">
        												<a class="nav-link" href="<?php echo base_url()?>admin/AdminController">Partner Travell Dashboard</a>
        											</li>
        											<li class="nav-item">
        												<a class="nav-link"
        													href="<?php echo base_url()?>admin/AdminController/product">Daftar Paket booking</a>
        											</li>
        										</ul>
        									</div>
        								</li>
        							</ul>
        						</div>
        					</li>
        					<li class="nav-item">
        						<a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false"
        							data-target="#submenu-5" aria-controls="submenu-5"><i
        								class="fas fa-fw fa-table"></i>List Data</a>
        						<div id="submenu-5" class="collapse submenu">
        							<ul class="nav flex-column">
        								<li class="nav-item">
        									<a class="nav-link" href="<?php echo base_url()?>admin/tbdata_user">Data User</a>
        								</li>
        								<li class="nav-item">
        									<a class="nav-link" href="<?php echo base_url()?>admin/tbdata_stok">Report Data Order</a>
        								</li>
        							</ul>
        						</div>
        					</li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo base_url()?>admin/bukti_tf"><i class="fab fa-trello"></i>Bukti Transfer</a>
							</li>
							<li class="nav-item">
                                <a class="nav-link" href="<?php echo base_url()?>admin/AdminController/edit_home"><i class="fab fa-pensil"></i>Edit Home</a>
                            </li>
        					<li class="nav-item">
        						<a class="nav-link" href="<?php echo base_url()?>Login/logout"><i
        								class="fas fa-sign-out-alt"></i>Log out</a>
        					</li>


        				</ul>
        			</div>
        			</li>
        			</ul>
        	</div>
        	</nav>
        </div>
        </div>
        <!-- ============================================================== -->
        <!-- end left sidebar -->
        <!-- ============================================================== -->
