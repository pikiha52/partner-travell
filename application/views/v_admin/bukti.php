<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

<div class="dashboard-wrapper">
	<div class="container-fluid  dashboard-content">
		<!-- ============================================================== -->
		<!-- pageheader -->
		<!-- ============================================================== -->
		<div class="row">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<div class="page-header">
					<h2 class="pageheader-title">Bukti Transfer</h2>
					<div class="page-breadcrumb">
						<nav aria-label="breadcrumb">
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="<?php echo base_url()?>admin"
										class="breadcrumb-link">Partner Travell</a></li>
								<li class="breadcrumb-item active" aria-current="page">Bukti Transfer</li>
							</ol>
						</nav>
					</div>
				</div>
			</div>
		</div>

		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
			<div class="card">
				<h5 class="card-header">Bukti Transfer</h5>
				<div class="card-body">
					<table class="table table-hover">
						<thead>
							<tr>
								<th scope="col">No</th>
								<th scope="col">Id Order</th>
								<th scope="col">Email</th>
								<th scope="col">File</th>
							</tr>
						</thead>
						<tbody>
							<?php $i = 1;?>
							<?php foreach($bukti as $u) : ?>
							<tr>
								<th><?= $i;?></th>
								<td><?php echo $u->id_order ?></td>
								<td><?php echo $u->email ?></td>
								<td>
									
									<div class="product-img">
									<img src="<?php echo base_url('/img/upload/').$u->gambar;?>" width="79"
											style="width:45%;cursor:pointer" onclick="onClick(this)"
											class="w3-hover-opacity">

											<div id="modal01" class="w3-modal" onclick="this.style.display='none'">
											<span
												class="w3-button w3-hover-red w3-xlarge w3-display-topright">&times;</span>
											<div class="w3-modal-content w3-animate-zoom">
												<img id="img01" style="width:50%">
												
											</div>
										</div>
									</div>
											<td>
											<a onclick="deleteConfirm('<?php echo site_url('admin/bukti_tf/delete/'.$u->id) ?>')"
												href="#!" class="btn btn-small text-danger"><i class="fas fa-trash"></i>
												Hapus</a>
											</td>
                                        </div></td>
							</tr>
							<?php $i++;?>
							<?php endforeach; ?>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
    <!-- Logout Delete Confirmation-->
	<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
		aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Are you sure?</h5>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">Data yang dihapus tidak akan bisa dikembalikan.</div>
				<div class="modal-footer">
					<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
					<a id="btn-delete" class="btn btn-danger" href="#">Delete</a>
				</div>
			</div>
		</div>
	</div>
	<script>
		function deleteConfirm(url) {
			$('#btn-delete').attr('href', url);
			$('#deleteModal').modal();
		}

	function onClick(element) {
		document.getElementById("img01").src = element.src;
		document.getElementById("modal01").style.display = "block";
	}
</script>