<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

	<!-- Title  -->
	<title>
		<?= $title; ?>
	</title>

	<!-- Favicon  -->
	<link rel="icon" href="<?php echo base_url()?>img/travel-img/logotravel.jpeg">

	<!-- Core Style CSS -->
	<link rel="stylesheet" href="<?php echo base_url()?>css/core-style.css">
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="<?php echo base_url()?>bootstrap/css/bootstrap.css">
	<link href="<?php echo base_url()?>assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
	<link rel="stylesheet" href="<?php echo base_url()?>//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/style.css">
	
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="<?php echo base_url()?>js/jquery/jquery-ui.js"></script>

	<script>
  $( function() {
    $( "#date" ).datepicker({
      dateFormat: "yy-mm-dd"
    });
  } );
  </script>
</head>

<body>
	<!-- Search Wrapper Area Start -->
	<div class="search-wrapper section-padding-100">
		<div class="search-close">
			<i class="fa fa-close" aria-hidden="true"></i>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="search-content">
						<form action="#" method="get">
							<input type="search" name="search" id="search" placeholder="Type your keyword...">
							<button type="submit"><img src="<?php echo base_url()?>img/core-img/search.png"
									alt=""></button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Search Wrapper Area End -->

	<!-- ##### Main Content Wrapper Start ##### -->
	<div class="main-content-wrapper d-flex clearfix">

		<!-- Mobile Nav (max width 767px)-->
		<div class="mobile-nav">
			<!-- Navbar Brand -->
			<div class="amado-navbar-brand">
				<a href="<?php echo base_url()?>toko/home"><img
						src="<?php echo base_url()?>img/travel-img/logotravel.jpeg" alt=""></a>
			</div>
			<!-- Navbar Toggler -->
			<div class="amado-navbar-toggler">
				<span></span><span></span><span></span>
			</div>
		</div>

		<!-- Header Area Start -->
		<header class="header-area clearfix">
			<!-- Close Icon -->
			<div class="nav-close">
				<i class="fa fa-close" aria-hidden="true"></i>
			</div>
			<!-- Logo -->
			<div class="logo">
				<a href="<?php echo base_url()?>toko/home"><img
						src="<?php echo base_url()?>img/travel-img/logotravel.jpeg" "></a>
            </div>
			<!-- Amado Nav -->
		
			
					<?php if($this->session->userdata('role_id')==='2'):?>
            <nav class=" amado-nav">
					<ul>
						<li><a href="<?php echo base_url()?>user/Profile">Profile</a></li>
						<li><a href="<?php echo base_url()?>UserController">Home</a></li>
						<!-- <li><a href="<?php echo base_url()?>UserController/contact">Contact</a></li> -->
                        <li><a href="<?php echo base_url()?>UserController/map">Map</a></li>
						<li><a href="<?php echo base_url()?>user/B_travel">Booking Travel</a></li>
						<!-- <li><a href="<?php echo base_url()?>userr/cart">Booking Tiket Pesawat</a></li> -->
						<li><a href="<?php echo base_url()?>Login/logout">Logout</a></li>
					</ul>
					</nav>
					<?php else:?>
						<nav class=" amado-nav">
					<ul>
                    <li><a href="<?php echo base_url()?>Login">Login</a></li>
						<li><a href="<?php echo base_url()?>UserController">Home</a></li>
                        <!-- <li><a href="<?php echo base_url()?>UserController/contact">Contact</a></li> -->
                        <li><a href="<?php echo base_url()?>UserController/map">Map</a></li>
					</ul>
					</nav>
					<?php endif;?>
					<!-- Social Button -->
					<div class="social-info d-flex justify-content-between">
						<a href="#"><i class="fab fa-facebook-f"></i></i></a>
						<a href="#"><i class="fab fa-twitter"></i></a>
						<a href="#"><i class="fab fa-instagram"></i></a>
						<a href="#"><i class="fab fa-whatsapp"></i></a>
					</div>
		</header>
		<!-- Header Area End -->