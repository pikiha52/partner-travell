        <!-- ##### Footer Area Start ##### -->
        <footer class="footer_area clearfix">
        <div class="container">
            <div class="row align-items-center">
                <!-- Single Widget Area -->
                <div class="col-12 col-lg-4">
                    <div class="single_widget_area">
                        <!-- Logo -->
                        <div class="footer-logo mr-50">
                            <!-- <a href="index.html"><img src="<?php echo base_url()?>img/travel-img/logotravel.jpeg" alt=""></a> -->
                        </div>
 
                         <p class="copywrite">Kami menyediakan jasa travell dengan harga yang terjangkau
                             dan banyak paket booking yang sangat menjanjikan, More info bisa <a href="https://wa.me/089522616459/?text=Saya%20ingin%20bertanya%20tentang%20paket%20travel,%20yang%20ada%20di%20Partner%20Travell">langsung klik.</a>
</p>
                    </div>
                </div>
                <!-- Single Widget Area -->
                <div class="col-12 col-lg-8">
                    <div class="single_widget_area">
                        <!-- Footer Menu -->
                        <!-- <div class="footer_menu">
                            <nav class="navbar navbar-expand-lg justify-content-end">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#footerNavContent" aria-controls="footerNavContent" aria-expanded="false" aria-label="Toggle navigation"><i class="fa fa-bars"></i></button>
                                <div class="collapse navbar-collapse" id="footerNavContent">
                                    <ul class="navbar-nav ml-auto">
                                        <li class="nav-item active">
                                            <a class="nav-link" href="<?php echo base_url()?>UserController">Home</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="<?php echo base_url()?>User">Booking Travel</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="<?php echo base_url()?>toko/cart">Booking Tiket Pesawat</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="<?php echo base_url()?>toko/about">About</a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- ##### Footer Area End ##### -->

    <!-- ##### jQuery (Necessary for All JavaScript Plugins) ##### -->
    <script src="<?php echo base_url()?>js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="<?php echo base_url()?>js/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
    <!-- Plugins js -->
    <script src="<?php echo base_url()?>js/plugins.js"></script>
    <!-- Active js -->
    <script src="<?php echo base_url()?>js/active.js"></script>

</body>

</html>