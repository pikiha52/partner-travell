<title>
	<?= $title?>
</title>

<link rel="icon" href="<?php echo base_url()?>img/travel-img/logotravel.jpeg">

<link href="<?php echo base_url()?>bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link href="<?php echo base_url()?>bootstrap/css/login.css" rel="stylesheet" id="bootstrap-css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="<?php echo base_url()?>bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>js/jquery-2.2.4.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<div class="wrapper fadeInDown">
	<div id="formContent">
		<div class="float-left">
		</div>
		<!-- Tabs Titles -->

		<!-- Icon -->
		<div class="fadeIn first">
			<img src="<?php echo base_url()?>img/travel-img/logotravel.jpeg" id="icon" alt="User Icon" />
		</div>

		<!-- Login Form -->
		<?php $this->session->flashdata('message'); ?>
		<form class="login100-form validate-form p-b-33 p-t-5" method="post"
			action="<?= base_url().'Login/proses_login'; ?>">
			<center>
				<div class="mb-2">
					<input type="textt" name="email" id="email" class="form-control  col-md-10"
						style="text-align: center" placeholder="email">
				</div>
				<div class="mb-2">
					<input type="password" value="*****" id="password" name="password" style="text-align: center" 
            class="form-control  col-md-10" ><br>
              <small>
                <input type="checkbox" onclick="myFunction()">Show Password
              </small>
				</div>
			</center>
			<input type="submit" class="fadeIn fourth" value="Login">
		</form>

		<div id="formFooter">
			<a class="btn btn-light" href="<?php echo base_url()?>UserController"><i class="fa fa-chevron-circle-left"></i>
				<small>back</small></a>
			<a class="btn btn-light" href="<?php echo base_url()?>Login/register"><i class="fa fa-user-plus"></i>
				<small>register</small></a>
		</div>

	</div>
</div>

<script>
	function myFunction() {
		var x = document.getElementById("password");
		if (x.type === "password") {
			x.type = "textt";
		} else {
			x.type = "password";
		}
	}

</script>
