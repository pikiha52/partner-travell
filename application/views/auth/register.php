<title>
	<?=$title?>
</title>

<link rel="icon" href="<?php echo base_url()?>img/travel-img/logotravel.jpeg">

<link href="<?php echo base_url()?>bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link href="<?php echo base_url()?>bootstrap/css/login.css" rel="stylesheet" id="bootstrap-css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="<?php echo base_url()?>bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>js/jquery-2.2.4.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<div class="wrapper fadeInDown">
	<div id="formContent">
		<!-- Tabs Titles -->

		<!-- Icon -->
		<div class="fadeIn first">
			<img src="<?php echo base_url()?>img/travel-img/logotravel.jpeg" id="icon" alt="User Icon" />
		</div>

		<!-- Login Form -->
		<form class="user" method="post" action="<?= base_url('login/register'); ?>">
			<center>
				<div class="mb-2">
					<input type="textt" name="full_name" id="full_name" class="form-control  col-md-10"
						style="text-align: center" placeholder="full name">
				</div>
				<div class="mb-2">
					<input type="textt" name="username" id="username" class="form-control  col-md-10"
						style="text-align: center" placeholder="username">
				</div>
				<div class="mb-2">
					<input type="textt" name="email" id="email" class="form-control  col-md-10"
						style="text-align: center" placeholder="email">
				</div>
				<div class="mb-2">
					<input type="textt" name="phone" id="phone" class="form-control  col-md-10"
						style="text-align: center" placeholder="no handphone">
				</div>
				<div class="mb-2">
					<input type="password" name="password" id="password" class="form-control  col-md-10"
            style="text-align: center" placeholder="password"><br>
            <small>
                <input type="checkbox" onclick="myFunction()">Show Password
              </small>
				</div>
			</center>
			<input type="submit" class="fadeIn fourth" value="Create">
		</form>


		<div id="formFooter">
			<a class="btn btn-light" href="<?php echo base_url()?>login"><i class="fa fa-chevron-circle-left"></i>
				<small>back</small></a>
		</div>

	</div>
</div>

<script>
	function myFunction() {
		var x = document.getElementById("password");
		if (x.type === "password") {
			x.type = "textt";
		} else {
			x.type = "password";
		}
	}

</script>