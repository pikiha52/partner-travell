        <!-- Product Catagories Area Start -->
        <div class="products-catagories-area clearfix">
            <div class="amado-pro-catagory clearfix">
            <?= $this->session->flashdata('message'); ?>
                <!-- Single Catagory -->
                   <?php foreach($project as $p) : ?>
                <div class="single-products-catagory clearfix">
                    <a href="<?php echo base_url()?>user/home">
                        <img src="<?php echo base_url().'img/travel-img/' .$p->gambar;?>">
                        <!-- Hover Content -->
                        <div class="hover-content">
                            <div class="line"></div>
                            <p><?=$p->caption?></p>
                            <h4><?=$p->nama?></h4>
                        </div>
                    </a>
                </div>
                <?php endforeach ;?>
                </div>
            </div>
                   </div>