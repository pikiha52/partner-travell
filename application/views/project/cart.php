<div class="cart-table-area section-padding-100">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-lg-8">
                        <div class="cart-title mt-50">
                            <h2>Booking Travel</h2>
                        </div>

                        <div class="cart-table clearfix">
                            <table class="table table-responsive">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Nama</th>
                                        <th>Harga</th>
                                        <th>Caption</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($this->cart->contents() as $items) :  $i++;
                                    ?>
                                    <tr>
                                        <td class="cart_product_img">
                                        <img src="<?php echo base_url().'img/travel-img/' .$items['gambar']?>">
                                        </td>
                                        <td class="cart_product_desc">
                                            <h5><?= $items['name'] ?></h5>
                                        </td>
                                        <td class="price">
                                            <span><?= number_format($items['price'],0,',','.') ?> </span>
                                        </td>
                                       <td><?= $items['caption'] ?></td>
                                    </tr>

                                    <div class="col-12 col-lg-4">
                        <div class="cart-summary">
                            <h5>Cart Total</h5>
                            <ul class="summary-table">
                                <li><span>subtotal:</span> <span><?= number_format($items['price'],0,',','.') ?></span></li>
                                <li><span>delivery:</span> <span>Free</span></li>
                                <li><span>total:</span> <span><?= number_format($items['price'],0,',','.') ?></span></li>
                            </ul>
                            <div class="cart-btn mt-100">
                                <a href="cart.html" class="btn amado-btn w-100">Checkout</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endforeach; ?>