        <div class="cart-table-area section-padding-100">
        	<style>
        		.bd-placeholder-img {
        			font-size: 1.125rem;
        			text-anchor: middle;
        			-webkit-user-select: none;
        			-moz-user-select: none;
        			-ms-user-select: none;
        			user-select: none;
        		}

        		@media (min-width: 768px) {
        			.bd-placeholder-img-lg {
        				font-size: 3.5rem;
        			}
        		}
        	</style>
        	<link href="<?php echo base_url()?>bootstrap/css/form-validation.css" rel="stylesheet">
        	</head>

        	<body class="bg-light">
        		<div class="container">
        			<?php
                  foreach ($this->cart->contents() as $items) :
                    ?>
        			<?php endforeach; ?>

        			<div class="row">

        				<div class="col-md-8 order-md-1">
        					<h4 class="mb-3">Billing address</h4>
        					<form method="post" action="<?= base_url('user/cart/order'); ?>">
        						<div class="row">
        							<div class="col-md-12 mb-3">
										<label for="">Nama</label>
        								<input type="text" class="form-control" name="full_name" value="<?php echo $this->session->userdata('full_name'); ?>">
        							</div>
        							<div class="col-12 mb-3">
										<label for="">Email</label>
        								<input type="email" class="form-control" name="email" value="<?php echo $this->session->userdata('email');?>">
        							</div>
        							<div class="col-12 mb-3">
										<label for="">Alamat</label>
        								<input type="text" class="form-control" name="alamat"
        									placeholder="">
        							</div>
        							<div class="col-12 mb-3">
										<label for="">Kota</label>
        								<input type="text" class="form-control" name="kota" placeholder="">
        							</div>
        							<div class="col-md-6 mb-3">
										<label for="">Kode pos</label>
        								<input type="text" class="form-control" name="kode_pos" placeholder="">
									</div>
        							<div class="col-md-6 mb-3">
										<label for="">Nomor handphone</label>
        								<input type="text" class="form-control" name="no_hp" placeholder="">
        							</div>
        							<div class="col-12 mb-3">
        								<label>Tanggal booking</label>
        								<input type="text" class="form-control" id="date" name="date">
        							</div>
        						</div>
        						<div class="col-md-4 order-md-2 mb-4">
        							<h4 class="d-flex justify-content-between align-items-center mb-3">
        								<span class="text-muted">Your cart</span>
        							</h4>

        							<div>
        								<h6 class="mb-8">Product name</h6>
        								<input type="text" class="form-control" name="nama" value="<?= $items['name']?>"
        									readonly placeholder="<?= $items['name']?>">
        							
        							<!-- <div>
        								<h6 class="my-0">Harga</h6>
        								<span name="price"
        									class="text-muted"><span><?= number_format($items['price'],0,',','.') ?></span>
        							</div> -->
        							<!-- <li class="list-group-item d-flex justify-content-between"> -->
        								<span>Harga</span>
        								<input type="text" class="form-control" name="price" readonly
        									value="<?= number_format($items['price'],0,',','.') ?>"
        									placeholder="<?= number_format($items['price'],0,',','.') ?>">
											<!-- </div> -->
									<!-- </li> -->
									 <hr class="mb-8">
        							<button class="btn amado-btn w-80" type="submit">Continue to checkout</button>
        					</form>
        				</div>
        			</div>