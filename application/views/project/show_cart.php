<div class="cart-table-area section-padding-100">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 col-lg-12">
				<div class="cart-table clearfix">
					<table class="table table-responsive">
						<thead>
							<tr>
								<th></th>
								<th>Booking</th>
								<th>Harga</th>
								<th>Caption</th>
							</tr>
                        </thead>
							<tbody>
								<?php
                                    foreach ($this->cart->contents() as $items) :
                                    ?>
								<tr>
									<td class="cart_product_img">
										<img src="<?php echo base_url().'img/travel-img/' .$items['image']?>">
									</td>
									<td class="cart_product_desc">
										<h5><?= $items['name'] ?></h5>
									</td>
									<td class="price">
										<span><?= number_format($items['price'],0,',','.') ?></span>
									</td>
									<td class="cart_product_desc">
										<h5><?= $items['caption'] ?></h5>
									</td>
								</tr>
							</tbody>
							<?php endforeach; ?>
							<div class="cart-btn mt-100">
								<a href="<?php echo base_url('user/cart/order');?>"
									class="btn amado-btn w-100">Checkout</a>
							</div>
                    </table>
                    <a href="<?php echo base_url('user/cart/clear_cart');?>"
									class="btn amado-btn w-100">Clear</a>
				</div>
			</div>
		</div>
	</div>
</div>
