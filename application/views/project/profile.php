<link rel="stylesheet" href="<?php echo base_url()?>assets/vendor/bootstrap/css/bootstrap.min.css">
  <link href="<?php echo base_url()?>assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/libs/css/style.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/vendor/charts/chartist-bundle/chartist.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/vendor/charts/morris-bundle/morris.css">
  <link rel="stylesheet"
    href="<?php echo base_url()?>assets/vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/vendor/charts/c3charts/c3.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/vendor/fonts/flag-icon-css/flag-icon.min.css">

<style>

	.emp-profile {
		padding: 3%;
		margin-top: 3%;
		margin-bottom: 3%;
		border-radius: 0.5rem;
		background: #fff;
	}

	.profile-img {
		text-align: center;
	}

	.profile-img img {
		width: 70%;
		height: 100%;
	}

	.profile-img .file {
		position: relative;
		overflow: hidden;
		margin-top: -20%;
		width: 70%;
		border: none;
		border-radius: 0;
		font-size: 15px;
		background: #212529b8;
	}

	.profile-img .file input {
		position: absolute;
		opacity: 0;
		right: 0;
		top: 0;
	}

	.profile-head h5 {
		color: #333;
	}

	.profile-head h6 {
		color: #0062cc;
	}

	.profile-edit-btn {
		border: none;
		border-radius: 1.5rem;
		width: 70%;
		padding: 2%;
		font-weight: 600;
		color: #6c757d;
		cursor: pointer;
	}

	.proile-rating {
		font-size: 12px;
		color: #818182;
		margin-top: 5%;
	}

	.proile-rating span {
		color: #495057;
		font-size: 15px;
		font-weight: 600;
	}

	.profile-head .nav-tabs {
		margin-bottom: 5%;
	}

	.profile-head .nav-tabs .nav-link {
		font-weight: 600;
		border: none;
	}

	.profile-head .nav-tabs .nav-link.active {
		border: none;
		border-bottom: 2px solid #0062cc;
	}

	.profile-work {
		padding: 14%;
		margin-top: -15%;
	}

	.profile-work p {
		font-size: 12px;
		color: #818182;
		font-weight: 600;
		margin-top: 10%;
	}

	.profile-work a {
		text-decoration: none;
		color: #495057;
		font-weight: 600;
		font-size: 14px;
	}

	.profile-work ul {
		list-style: none;
	}

	.profile-tab label {
		font-weight: 600;
	}

	.profile-tab p {
		font-weight: 600;
		color: #0062cc;
	}
</style>
<div class="products-catagories-area clearfix">
				<div class="container emp-profile">
				<?php 
        if(isset($error))
        {
            echo "ERROR UPLOAD : <br/>";
            print_r($error);
            echo "<hr/>";
        }
        ?>
					<div class="row">
						<div class="col-md-6">
							<div class="profile-head">
							<?= $this->session->flashdata('message'); ?>
								<h5>
									<?php echo $this->session->userdata('full_name');?>
								</h5>
								<ul class="nav nav-tabs" id="myTab" role="tablist">
									<li class="nav-item">
										<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home"
											role="tab" aria-controls="home" aria-selected="true">Pesanan</a>
									</li>
								</ul>

								<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
									<table class="table table-hover">
										<thead>
											<tr>
												<th scope="col">No</th>
												<th scope="col">Nomor Order</th>
												<th scope="col">Nama</th>
												<th scope="col"></th>
												<!-- <th scope="col">Email</th> -->
												<th scope="col">Paket Booking</th>
												<th scope="col">Tanggal Keberangkatan</th>
												<th scope="col">Harga</th>
												<th scope="col">Upload bukti Transfer</th>
												<th scope="col">Status</th>
											</tr>
										</thead>
										<tbody>
											<?php $i = 1 ;?>
											<?php foreach($order as $u) : ?>
											<tr>
												<td><?=$i;?></td>
												<td><?php echo $u->id_order ?></td>
												<td><?php echo $u->full_name ?></td>
												<td><?php echo $u->email ?></td>
												<td><?php echo $u->nama ?></td>
												<td><?php echo $u->date ?></td>
												<td><?php echo $u->price ?></td>
												<td><a data-toggle="modal" data-target="#addModal"
														class="btn btn-primary">Upload</a></td>
												<td><?php echo $u->status ?></td>
												<td><a href="" type="button" class="btn btn-light">asa</a></td>
											</tr>
											<?php $i++;?>
											<?php endforeach; ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			<!-- </div> -->
		</div>
	</div>
 <!-- Modal Add Product-->
 <form method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>user/profile/proses">
        <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Upload Bukti Transfer</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
             
                <div class="form-group">
                    <label>ID Order</label>
                    <input type="text" class="form-control" name="id_order" placeholder="ID Order">
                </div>
                 
                <div class="form-group">
                    <label>Upload</label>
                    <input type="file" class="form-control" name="file">
                </div>
 
				<div class="form-group">
                    <label>Email</label>
                    <input type="email" class="form-control" name="email">
                </div>
             
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            </div>
        </div>
        </div>
    </form>

 
	<script src="<?php echo base_url()?>assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <!-- bootstap bundle js -->
    <script src="<?php echo base_url()?>assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <!-- slimscroll js -->
    <script src="<?php echo base_url()?>assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <!-- main js -->
    <script src="<?php echo base_url()?>assets/libs/js/main-js.js"></script>
    <!-- chart chartist js -->
    <script src="<?php echo base_url()?>assets/vendor/charts/chartist-bundle/chartist.min.js"></script>
    <!-- sparkline js -->
    <script src="<?php echo base_url()?>assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
    <!-- morris js -->
    <script src="<?php echo base_url()?>assets/vendor/charts/morris-bundle/raphael.min.js"></script>
    <script src="<?php echo base_url()?>assets/vendor/charts/morris-bundle/morris.js"></script>
    <!-- chart c3 js -->
    <script src="<?php echo base_url()?>assets/vendor/charts/c3charts/c3.min.js"></script>
    <script src="<?php echo base_url()?>assets/vendor/charts/c3charts/d3-5.4.0.min.js"></script>
    <script src="<?php echo base_url()?>assets/vendor/charts/c3charts/C3chartjs.js"></script>
    <script src="<?php echo base_url()?>assets/libs/js/dashboard-ecommerce.js"></script>
<script src="/js/jquery.min.js"></script>
<script src="/js/bootstrap.bundle.min.js"></script>