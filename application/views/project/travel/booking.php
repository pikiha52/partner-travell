<div class="amado_product_area section-padding-100">
<?= $this->session->flashdata('message'); ?>
	<div class="row">
		<div class="col-12">
			<?php
//Columns must be a factor of 12 (1,2,3,4,6,12)
$numOfCols = 3;
$rowCount = 0;
$bootstrapColWidth = 12 / $numOfCols;
?>
			<div class="row">
				<?php
foreach ($product as $product){
?>
				<div class="col-md-<?php echo $bootstrapColWidth; ?>">
					<div class="single-product-wrapper">
						<!-- Product Image -->
						<div class="product-img">
							<img src="<?php echo base_url().'img/travel-img/' .$product->gambar;?>">
							<!-- Hover Thumb -->
						</div>

						<!-- Product Description -->
						<div class="product-description d-flex align-items-center justify-content-between">
							<!-- Product Meta Data -->
							<div class="product-meta-data">
								<div class="line"></div>
								<a href="<?php echo base_url()?>UserController/detail">
									<h6><?=$product->nama?></h6>
								</a>
								<p><?=$product->caption?></p>
								<p class="product-price">Rp.<?=$product->harga?></p>
							</div>
							<p>
								<?=anchor('user/cart/add_to_cart/' . $product->id_product, 'Booking' , [
      'class' => 'btn btn-primary',
      'role' => 'button'
     ])?>
							</p>
						</div>
					</div>
				</div>
				<?php
    $rowCount++;
    if($rowCount % $numOfCols == 0) echo '</div><div class="row">';
}
?>
			</div>
		</div>
	</div>
</div>
</div>