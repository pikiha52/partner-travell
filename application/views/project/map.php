        <!-- Product Catagories Area Start -->
        <div class="products-catagories-area clearfix">
        	<div class="amado-pro-catagory clearfix">
        		<div class="page-header">
                    <h2 class="pageheader-title">Lokasi Kami</h2>
                    <p>Jika anda ingin melakukan booking secara offline silahkan datang ketoko kami,<br>
                dan apabila ada yang ingin ditanyakan secara mendetail silahkan hubungi contact yang tertera<br>
            atau langsung datang ketempat kami</p>
        			<iframe
        				src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.9891336430837!2d106.90796146433368!3d-6.265158595465218!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f32af6703c71%3A0xf89cd7f58da5243f!2sUniversitas%20BSI%20Jatiwaringin!5e0!3m2!1sid!2sid!4v1589814536088!5m2!1sid!2sid"
        				width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""
        				aria-hidden="false" tabindex="0" class="gmaps" ></iframe>
        		</div>
        	</div>
        </div>
